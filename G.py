# -*- coding: UTF-8 -*-
########################
# FILEWIDE GLOBAL DATA
########################
OUT_COUNT = 0
SKILLS = {}
STATUSES = {}
TALENTS = {}
SUMMONS = {}
PROFESSIONS = {
    # melee
    "Soldier": None,
    "Outlaw": None,
    "Guardian": None,
    # hybrid
    "Engineer": None,
    "Performer": None,
    "Priest": None,
    # magic
    "Scholar": None,
    "Shaman": None,
    "Spellhand": None
}

SCHOOLS = {
    # Basic Schools
    "Enemy Skills":      ["ES", "A"],
    "Race Skills":       ["RS", "B"],
    # Melee Schools
    "Black Fang":        ["BF", "C"],
    "Dancing Blade":     ["DB", "D"],
    "Praying Mantis":    ["PM", "E"],
    "Metallic Swarm":    ["MS", "F"],
    "Falling Star":      ["FS", "G"],
    "Glorious Union":    ["GU", "H"],
    "Heroic Champion":   ["HC", "I"],
    "Hidden Serpent":    ["HS", "J"],
    "Iron Fortress":     ["IF", "K"],
    # Magic Schools
    "Captivation":       ["ca", "L"],
    "Instruction":       ["in", "M"],
    "Cultivation":       ["cu", "N"],
    "Destruction":       ["de", "O"],
    "Manipulation":      ["ma", "P"],
    "Corruption":        ["co", "Q"],
    "Augmentation":      ["au", "R"],
    "Purification":      ["pu", "S"],
    "Creation":          ["cr", "T"],
    "Fabrication":       ["fa", "U"],
    "Precipitation":     ["pr", "V"]
    # Exotic Schools
    #"Vindictive Drive":  ["VD", "W"],
    #"Serene First":      ["SF", "X"],
    #"????":              ["??", "Y"],
    #"$$$$":              ["$$", "Z"]
}

AR = {"=>":"→",  "=^":"⬏"} # ⮭ ⮥ ⬏ ⤴ ↗ → → 🠈 🠊 🠉 🠋 ⮜ ⮞ ⮝ ⮟
STAR = "★"
DOT = "•"

LEVELS = {
    1:  "Rookie",
    2:  "Talented",
    3:  "Professional",
    4:  "Veteran",
    5:  "Legendary"
}

COLORS = {
    "Profession":   ["dark",    "normal",  "neon",    "light"],
    # melee
    "Soldier":      ["#1f180d", "#c59c6d", "#ffc871", "#cfb395"],
    "Outlaw":       ["#010601", "#0db00d", "#1eff00", "#abe8ab"], 
    "Guardian":     ["#21201c", "#d3bc1a", "#fffc00", "#fff4a6"],
    # hydrid
    "Engineer":     ["#3e151c", "#b94e4e", "#ff0000", "#fcc7c7"],
    "Performer":    ["#241129", "#b2499b", "#f117ff", "#ffbef1"],
    "Priest":       ["#2e160b", "#f15c23", "#ffb30f", "#f9c0a1"],
    # magic
    "Scholar":      ["#0e1a25", "#4089c9", "#00fcff", "#c1daf2"],
    "Shaman":       ["#1a2a15", "#3bb485", "#36ffbf", "#c6ffe9"],
    "Spellhand":    ["#260008", "#d61b42", "#ff178f", "#ffa8ba"],
    # redacted
    "Laborer":      ["#221e1b", "#aa9180", "#fbe9dc", "#ceb8a9"],
    "Hunter":       ["#0a0810", "#331985", "#6000ff", "#b5a6d0"],
    "Brawler":      ["#323535", "#97a3a4", "#8ab7b9", "#cbcbcb"],
    "Dragoon":      ["#404527", "#9eb72a", "#c6ff00", "#f0fcb7"]
}

HUES = {
    "red":      0,
    "orange":   28,
    "yellow":   58,
    "green":    119,
    "blue":     200,
    "indigo":   240,
    "purple":   275,
    "violet":   293,
    "pink":     311
}


COLOR = {
    "white":    "#ffffff",# 2
    "green":    "#afff91",# 3
    "yellow":   "#faff61",# 4
    "orange":   "#dba73d",# 5
    "pink":     "#ffb0fc",# 6
    "red":      "#c20000",# 7
    "purple":   "#430082",# 8
    "blue":     "#0066ba",# 9
    "teal":     "#001466",# 10
    "black":    "#000000",# 11
    "none":     "#ff00ff", # missing texture
    "cyan":     "#00ffff"
}


POWER_COLORS = [
    "none",     # 1
    "white",    # 2
    "green",    # 3
    "yellow",   # 4
    "orange",   # 5
    "pink",     # 6
    "red",      # 7
    "purple",   # 8
    "blue",     # 9
    "teal",     # 10
    "black"     # 11
]

CONTEXT_BL = [
    "Smash",
    "Slice",
    "Heat",
    "Chill",
    "Jump",
    "Move"
]