# a python launcher to run/debug
from printer import menu, clear, waitFor
from os import system, listdir

def setup():
	system("chcp 65001")
	clear()
	files = listdir()
	blacklist = ["G.py", "_run.py", "printer.py", "assets.py"]
	files = [x for x in files if x.endswith(".py") and x not in blacklist]
	return files


def selection(files):
	print("Choose an entry point:")
	target = menu(files)
	return target


def main(target, fastClose=False):
	clear()
	system("py "+target)

	if fastClose:
		return True


	print()
	print("PRESS [enter] to run {} again...".format(target))
	print("PRESS [backspace] to choose new entry point.")

	ans = waitFor([b'\r',b'\x08'])
	if ans == b'\r':
		return True
	elif ans == b'\x08':
		return False


if __name__ == "__main__":
	while True:
		clear()
		print("Would you like fastClose?")
		fastCloseMode = menu([True, False])
		clear()
		files = setup()
		target = selection(files)
		runAgain = True
		while runAgain:
			runAgain = main(target, fastCloseMode)