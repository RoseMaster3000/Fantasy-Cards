# images, fonts, assets (used for card creation)
from PIL import Image, ImageFont
from os import listdir

def fetch_imgs(folderName):
    fetchDict = {}
    size = []
    for fname in listdir(folderName):
        if not fname.endswith("png"): continue
        key = fname.replace(".png", "")
        fetchDict[key] = Image.open("{}\\{}".format(folderName,fname))
        if size == []: size = fetchDict[key].size
    return fetchDict#,size

def fetch_icns(folders=None):
	icns = {}
	root = ""
	if folders == None: listdir("assets\\icons")
	for folder in folders:
		iconCount = 0
		if folder[0]=="_": folderName = folder[1:]
		else: folderName = folder
		for fname in listdir("assets\\icons\\"+folder):
			name = fname[0:-4]
			if folderName not in icns: icns[folderName] = dict()
			icns[folderName][name] = Image.open("assets\\icons\\{}\\{}".format(folder,fname)).convert("RGBA")
			iconCount += 1
		#print("{: <16} │ {}".format(folderName, iconCount)) 
	return icns

PORTRAITS = fetch_imgs("assets\\portraits")
TEMPLATE = fetch_imgs("assets\\templates")
RACES = fetch_imgs("assets\\races")

ICONS = fetch_icns([
	"_custom",
	"_schools"
])



# FONTS
FONT = {
	# profession cards
	"pname":	ImageFont.truetype("assets/fonts/deutsch.ttf", 350),
	"plevel":	ImageFont.truetype("assets/fonts/deutsch.ttf", 140),	
	"pdesc":	ImageFont.truetype("assets/fonts/cambria-bold.ttf", 107),	
	"pschools":	ImageFont.truetype("assets/fonts/cambria-bold-itallic.ttf", 100),	
	"pdecks":	ImageFont.truetype("assets/fonts/cambria-bold.ttf", 170),
	"pcount":	ImageFont.truetype("assets/fonts/cambria-bold.ttf", 150),
	"flow":		ImageFont.truetype("assets/fonts/deutsch.ttf", 260),
	"ppflow":	ImageFont.truetype("assets/fonts/cambria-bold.ttf", 110),
	"pflow":	ImageFont.truetype("assets/fonts/cambria-bold-itallic.ttf", 110),	
	# skill cards
	"stitle":	ImageFont.truetype("assets/fonts/deutsch.ttf", 300),
	"stars":	ImageFont.truetype("assets/fonts/emoji.ttf", 70),
	"arrows":   ImageFont.truetype("assets/fonts/emoji.ttf", 130),
	"bline":    ImageFont.truetype("assets/fonts/cambria-bold.ttf", 130),
	"line":		ImageFont.truetype("assets/fonts/cambria-bold.ttf", 115),
	"sline":    ImageFont.truetype("assets/fonts/cambria-bold-itallic.ttf", 95),
	"context":	ImageFont.truetype("assets/fonts/cambria-bold-itallic.ttf", 110),
	"sdesc":	ImageFont.truetype("assets/fonts/deutsch.ttf", 135),
	#"sdesc":	ImageFont.truetype("assets/fonts/matura.ttf", 135),
	"sschool":	ImageFont.truetype("assets/fonts/ancient.ttf", 210),
	"serial":	ImageFont.truetype("assets/fonts/cambria-bold.ttf", 80),
	# conditions cards
	"ctitle":	ImageFont.truetype("assets/fonts/deutsch.ttf", 360),
	"ctype":	ImageFont.truetype("assets/fonts/cambria-bold-itallic.ttf", 120),
	"cdesc":	ImageFont.truetype("assets/fonts/cambria-bold.ttf", 140),
	# race cards
	"rtitle":	ImageFont.truetype("assets/fonts/matura.ttf", 340),
	# summons card
	"sumtitle":	ImageFont.truetype("assets/fonts/matura.ttf", 270),
	"sumdesc":	ImageFont.truetype("assets/fonts/matura.ttf", 130),
	"sumhp":	ImageFont.truetype("assets/fonts/cambria-bold.ttf", 150),
	# status
	"stsPow":   ImageFont.truetype("assets/fonts/cambria-bold.ttf", 640)
}

