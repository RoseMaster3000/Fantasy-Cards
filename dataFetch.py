# fetch 2d lists from a CSV on disk or GOOGLE SHEETS
from __future__ import print_function
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pickle
import os.path

SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
WORKBOOKS = {
	"PLANNING": "1u0SMY2dcnAkM6YH1imhfCi7ie12iQuedq9W3wUOUrLc",
	"SKILLS": "15-zzLazgXJenxFfgw9AtQH-an9sWWIRxLmaRosFWsiA",
	"PROFESSIONS": "1ZQPedKJWWLQM5o36Z8b1VwNtm-_r2v3ladhMpQ4oH2A",
	"TALENTS": "1abucsSMWlxH7nSRNpkhQF59Gk4ZQvgJQR7pYNB8N9NQ",
	"STATUSES": "1nF9TwxjdF7PAshuuupJpaIFQEN-DjaAxAdhOFL9WZ28",
	"RACES": "1cyFdJ24ysLe3NRHV6lhpJsL25Qf3oUQVop_LAB3O878"
}

# fetch data from GOOGLE SHEETS
def getSheet(workbook, sheet, start="A", end="Z"):
	# prep inputs
	SPREADSHEET_ID = WORKBOOKS[workbook]
	RANGE_NAME = "{}!{}:{}".format(sheet,start,end)
	creds = None

	# try to load credentials
	if os.path.exists('credentials/token.pickle'):
		with open('credentials/token.pickle', 'rb') as token:
			creds = pickle.load(token)
	# If there are no (valid) credentials available, let the user log in + create them
	if not creds or not creds.valid:
		if creds and creds.expired and creds.refresh_token:
			creds.refresh(Request())
		else:
			flow = InstalledAppFlow.from_client_secrets_file('credentials/google.json', SCOPES)
			creds = flow.run_local_server()
		with open('credentials/token.pickle', 'wb') as token:
			pickle.dump(creds, token)

	# call sheets API
	service = build('sheets', 'v4', credentials=creds)
	sheet = service.spreadsheets()
	result = sheet.values().get(spreadsheetId=SPREADSHEET_ID, range=RANGE_NAME).execute()
	values = result.get('values', [])

	# return
	if not values:
		print("No data in",sheet)
		return None
	else:
		return values



# save local CSV file of data (presumably a 2D array from Google Sheets)
ROW_DELIM = "$$$"#;
COL_DELIM = "###"#/n
def writeSheet(workbook, sheet, values):
	filename = "csv/{}-{}.csv".format(workbook, sheet)
	content = []

	# convert list of lists into CSV string
	for line in values:
		content.append(COL_DELIM.join(line))
	content = ROW_DELIM.join(content)

	# write to file
	with open(filename, 'wb') as target_file:
		target_file.write(content.encode("utf-8"))
		return



# fetch data from local file [BUT] if no file getSheet() from google 
def readSheet(workbook, sheet, start="A", end="Z"):
	filename = "csv/{}-{}.csv".format(workbook, sheet)
	values = []

	if os.path.exists(filename):
		# read / return list of lists
		with open(filename, 'rb') as target_file:
			content = target_file.read().decode("utf-8")
			content = content.split(ROW_DELIM)
			for line in content:
				values.append(line.split(COL_DELIM))
		return values
	else:
		# no file, fetch Google Sheets
		print("{}-{}.csv not found => referring to Google Sheets".format(workbook, sheet))
		values = getSheet(workbook, sheet, start, end)
		writeSheet(workbook, sheet, values)
		return values



if __name__ == "__main__":
	data = readSheet("SKILLS", "RS") #fetch race skill
	for d in data:
		print(d)

	# data = getSheet("PLANNING", "Schools")
	# for d in data:
	# 	print(d)
