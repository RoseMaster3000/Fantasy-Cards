# handy data massaging functions
import G


def sanatizeSclStr(sclStr):
    if sclStr.title() in SCHOOLS.keys():
        return sclStr.title()
    for s in SCHOOLS:
        if type(G.SCHOOLS[s])==object:
            if sclStr.upper() in [s.key,s.letter]:
                return s
        elif type(G.SCHOOLS[s])==list:
            if sclStr.upper() in G.SCHOOLS[s]:
                return s
    raise KeyError('{} is not a known SCHOOL, KEY, or LETTER'.format(sclStr))


# filters skill objects of given school and level (returns dict/list/tuple)
def skillFilter(sclStr=None, lvl=None, typ=dict):
    # sanatize sclStr
    sclStr = sanatizeSclStr(sclStr)

    # sanatize lvl (integer)            
    if lvl!=None:
        if lvl in G.LEVELS:
            lvl = G.LEVELS.index(lvl.strip().capitalize())
        else:
            lvl = int(lvl)

    # prepare output
    if typ == dict: output = {}
    elif typ==list: output = []
    elif typ==tuple:output = ()
    else: raise Exception("{} is not a valid type for skillFilter()".format(typ))

    # sort and return
    for skill in G.SKILLS:
        if scl==None or skill.sclStr==sclStr:
            if lvl==None or skill.lvl==lvl:
                if   typ==dict:   output[skill.name] = skill
                elif typ==list:   output.append(skill)
                elif typ==tuple:  output.append(skill)
    return output




# reverse search dictionary value to find key
def findKey(mydict, value, index=None):
    for key in mydict:
        if index==None:
            if mydict[key]==value:
                return key
        else:
            if mydict[key][index]==value:
                return key
    raise Exception("findKey() failed to find \"{}\" in \"{}\"".format(value, mydict))



def peelList(ls):
    if type(ls)!=list:
        raise Exception("peelList() was not given a list")

    newList = []
    for i,item in enumerate(ls):
        if type(item) == list:
            newList.append(item[0])
        else:
            raise Exception("peelList() was not given a list of lists: index {} was a {}".format(i,type(item)))
    return newList



def column(ls, col):
    return [row[col] if len(row)>col else "" for row in ls]