from dataFetch import readSheet
from dataFilter import findKey, column
from dataObject import *
import G
import os, sys

def checkSchool(values):
    for v in values:
        if v == []:
            continue
        elif v[0] == "END":
            break
        elif v[0] == "-":
            return True
    return False


# populate (G.SCHOOLS) + (G.SKILLS)
def loadSchool(scl):
    # FETCH DATA
    key = G.SCHOOLS[scl][0]
    values = readSheet("SKILLS", key, "A", "A")
    
    # BUILD SCHOOL
    curSchool = School(name=scl, flavor=values[1][0], draw=values[2][0])

    # eject if no skills in school (yet)
    if checkSchool(values) == False: return curSchool

    # BUILD SKILLS
    content = []
    i = 3
    while True:
        if values[i][0] == "END":
            break
        # fetch level + get next NAME
        elif values[i][0] in G.LEVELS.values():
            level = findKey(G.LEVELS, values[i][0])
            name = values[i+1][0]
            i += 2

        # content line
        elif "=>" in values[i][0] or "=^" in values[i][0] or "──── or ────" in values[i][0]:
            content.append(values[i][0])
            i += 1

        # finalize skill + flavor + get next NAME
        elif values[i][0] == "-":
            flavor = values[i-1][0]
            Skill(name, level, scl, content, flavor)
            content = []
            name = values[i+1][0]
            # EXIT
            if name == "END":
                break
            # fetch level (get next NAME)
            elif name in G.LEVELS:
                level = findKey(G.LEVELS, name)
                name = values[i+2][0]
                i += 3
            # next skill
            else:
                i += 2

        # move forward
        else:
            i += 1
    return curSchool


# populate (G.PROFESSIONS)
def loadProfession(profName):
    # FETCH DATA
    values = readSheet("PROFESSIONS", profName, "A", "G")
    # LOAD OBJECT
    curProf = Profession(
        name = values[0][0],
        flavor = values[0][2],
        examples = values[2][2], 
        powerDecks = [
            column(values,2)[7:18],
            column(values,3)[7:18],
            column(values,4)[7:18],
            column(values,5)[7:18],
            column(values,6)[7:18]
        ],
        scls = column(values,1)[18:23],
        healthDecks = [
            column(values,2)[4:7],
            column(values,3)[4:7],
            column(values,4)[4:7],
            column(values,5)[4:7],
            column(values,6)[4:7]
        ],
        skillDecks = values[23][2:7],
        skillHands = values[24][2:7],
        flow = column(values,2)[25:29],
        flowName = values[25][1]
    )
    return curProf


def loadStatus(sheetName):
    statusCount = 0
    statusType = ""
    values = readSheet("STATUSES", sheetName, "B", "D")
    values = values[2:]
    hasCard = sheetName is not "Other"

    for line in values:
        if len(line)==0 or line[0] == "":
            continue
        elif line[0] == "Ideas":
            break
        elif line[0] in ["Node Obstacle", "Edge Obstacle"]:
            statusType = line[0]
        elif line[0] in ["Power Status","Health Status", "Skill Status"]:
            statusType = line[0].split(" ")[0]
            if "+" in sheetName: statusType += " Buff"
            elif "-" in sheetName: statusType += " Debuff"
        else:
            curStatus = Status(
                name = line[1],
                statusType = statusType,
                effect = line[2],
                hasCard = hasCard,
                iconRaw = line[0]
            )
            statusCount += 1
    return statusCount

def loadSummons():
    summonCount = 0
    values = readSheet("STATUSES", "Summons", "A", "B")
    for line in values[3:]:
        llen = len(line)
        if llen==0:
            continue
        elif line[0]=="Ideas":
            break            
        elif llen==1:
            curSummon.lines.append(line[0])
        elif llen==2 and line[1].startswith("HP"):
            curSummon = Summon(
                name=line[0],
                hp=line[1].split(":")[1].strip(),
                trigger="",
                lines=[],
                icon=""
            )
            summonCount+=1
        elif line[0].startswith("After"):
            curSummon.trigger = "{}, {} acts:".format(line[0],curSummon.name)
            #curSummon.icon = line[1]
            curSummon.setIconPath(line[1])

    return summonCount


def loadAll(refreshAll=False, verbose=True):
    if refreshAll:
        for f in os.listdir("csv"): os.remove("csv\\"+f)

    # load SCHOOLS/SKILLS
    for s in sorted(G.SCHOOLS):
        curSchool = loadSchool(s)
        if verbose:print("{: <16} │ {}".format(curSchool.name, curSchool.skillCount(True)))
    if verbose:print("──────────────────────────────────────────")
    # load PROFESSIONS
    for p in G.PROFESSIONS:
        curProf = loadProfession(p)
        if verbose:print("{: <16} │ {}".format(curProf.name, curProf.examples))
    if verbose:print("──────────────────────────────────────────")
    # load STATUSES / SUMMONS
    for s in ["+Status","-Status","Obstacles","Other"]:
        statusCount = loadStatus(s)
        if verbose:print("{: <16} │ {}".format(s, statusCount)) 
    summonCount = loadSummons()
    if verbose:print("{: <16} │ {}".format("Summons", summonCount)) 
    if verbose:print("──────────────────────────────────────────")  





if __name__ == "__main__":
    loadSchool("Race Skill")

    for key in G.SKILLS:
        G.SKILLS[key].print()
