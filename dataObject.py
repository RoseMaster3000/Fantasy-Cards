# Professions, Schools, Skills (objects)
from dataFetch import readSheet
from dataFilter import findKey, column
import G

class Profession:
    def __init__(self, name, flavor, examples, scls, healthDecks, powerDecks, skillDecks, skillHands, flow, flowName):
        self.name = name                        # string name of profession
        self.flavor = flavor                    # sting flavor text of profession
        self.description = flavor
        self.examples = examples                # list of string example characters

        self.scls = []                          # list of strings of schools available
        for s in scls:
            if s not in ["",[]]:
                self.scls.append(s)
        self.schools = []                       # list of objects of schools avaialble
        for s in self.scls:
            self.schools.append(G.SCHOOLS[s])
        
        self.healthDecks = healthDecks          # list of list of ints (number of flesh, will, fort)
        self.powerDecks = powerDecks            # list of lists of ints (power deck comps)
        self.skillDecks = skillDecks            # list of ints (max skill deck size)
        self.skillHands = skillHands            # list of ints (number of cards drawn)
        self.flow = flow                        # list of strings (skill flow)
        self.flowName = flowName.title()        # name of flow
        G.PROFESSIONS[name] = self              # CREATE reference in global dict
        # hard coded attributes
        self.colors = {
            "dark":    G.COLORS[self.name][0],
            "normal":  G.COLORS[self.name][1],
            "neon":    G.COLORS[self.name][2],
            "light":   G.COLORS[self.name][3]
        }
        self.portraitPath = "assets\\portraits\\"+self.name.lower()+".png"
        self.cardPath = "cards\\profession\\{}.png".format(self.name)

    def __str__(self):
        return "<Profession>({})".format(self.name)

    def renderFlow(self, lvl):
        #lvl -= 1
        bamt = [1,1,2,2,3]
        rFlow = []
        for f in self.flow:
            nf = f.replace("HAND SIZE", self.skillHands[lvl])
            nf = nf.replace("BAMT", str(bamt[lvl]))
            rFlow.append(nf)
        return rFlow


    def print(self):
        print(self.name+"─────────────────────────────────")
        print(self.flavor)
        print(self.examples)
        print("SCHOOL────────────────────────────────────────")
        for s in self.schools:
            print("{} - {}".format(s.name, s.skillCount()))
        print("POWER─────────────────────────────────────────")
        for p in self.powerDecks: print(p)
        print("HEALTH─────────────────────────────────────────")
        for h in self.healthDecks: print(h)
        print("SKILL DECK────────────────────────────────────")
        for x in self.skillDecks: print(x)
        print("SKILL HANDS───────────────────────────────────")
        for x in self.skillHands: print(x)
        print(self.flowName+"─────────────────────────────────")
        for x in self.flow: print(x)



class School:
    def __init__(self, name, flavor, draw):
        self.serial = 0
        # load hardcoded data from GLOB DICT
        self.key = G.SCHOOLS[name][0]       # string of 2 letter abbreviation
        self.letter = G.SCHOOLS[name][1]    # string of 1 letter abbreviation
        # load other data
        self.name = name                    # string of name of school
        self.flavor = flavor                # string of flavor text
        self.draw = int(draw.split(":")[1]) # standard draw rate of this school
        self.skills = []                    # list of skill object children
        # populate GLOB DICT
        #G.SCHOOLS[self.key] = self          # CREATE reference in global dict
        G.SCHOOLS[self.name] = self         # CREATE (redundant) reference in global dict
        # art
        self.iconPath = "assets\\icons\\_schools\\"+self.name.lower()+".png"


    def skillCount(self, zeroSpace=False):
        x = len(self.skills)
        if x==0 and zeroSpace: return ""
        else: return x

    def add(self, skill):
        self.skills.append(skill)

    def __str__(self):
        return "<School>({})".format(self.name)

    def __getitem__(self, ind):
        indType = type(ind)
        if indType == str:
            try: return self.__dict__[ind]
            except: return None
        elif indType == int:
            try: return self.skills[ind]
            except: return None         
        else:
            return None

class Skill():
    def __init__(self, name, levelInt, sclStr, content, flavor):
        self.name = name                    # string of skill
        self.lvl = levelInt                 # int level of skill
        self.lvl = levelInt                 # int level of skill
        self.level = G.LEVELS[levelInt]     # string level of skill
        self.scl = sclStr                   # string of parent school
        self.school = G.SCHOOLS[sclStr]     # object of parent school
        self.lines = content                # list of strings (skill lines)
        self.flavor = flavor                # string of flavor text
        self.school.serial += 1
        self.serial = "{}{:02}".format(self.school.letter,self.school.serial)
        if " " in self.scl:
            self.type="martial"
        else:
            self.type="magical"
        if name in G.SKILLS: raise Exception("There are two skills named ["+name+"]")
        G.SKILLS[name] = self               # CREATE reference in global dict
        self.school.add(self)               # CREATE reference in parent school
        self.cardPath = "cards\\skill\\{}.png".format(self.name)

    def __str__(self):
        return "<Skill>({})".format(self.name)

    # generates dict, KEY is every summon/status this skill does, VALUE is the amount
    def inflictDict(self):
        if hasattr(self, "infDict"): return self.infDict
        infDict = {}
        for line in self.lines:
            for s in G.STATUSES:
                if s in line and G.STATUSES[s].hasCard and "draw" not in line:
                    try:  amt = int(line.split("[")[1].split("]")[0])
                    except: amt = 1
                    if s in infDict:  infDict[s]+=amt
                    else: infDict[s]=amt

            for s in G.SUMMONS:
                if s in line:
                    if s in infDict:  infDict[s]+=1
                    else: infDict[s]=1

        self.infDict = infDict
        return infDict

    def print(self):
        print("{}({}): {}".format(self.school.key, self.level, self.name))
        print(self.flavor)
        for ln in self.content:
            print("\t",ln)


class Status:
    def __init__(self, name, statusType, effect, hasCard, iconRaw):
        self.name = name
        self.effect = effect
        self.type = statusType
        self.hasCard = hasCard
        self.iconRaw = iconRaw
        self.icon = "assets\\icons\\{}\\{}.png".format(self.iconRaw.split(":")[0], self.iconRaw.split(":")[1])
        if name in G.STATUSES: raise Exception("There are two statuses named ["+name+"]")
        if "XXX" in effect: print("\t{} still has [XXX]".format(self.name))
        G.STATUSES[name] = self
        self.cardPath = "cards\\status\\{}.png".format(self.name)
        self.tokenPath = "cards\\token\\{}.png".format(self.name)

    def print(self):
        print("{}({})::{}".format(self.name,self.statusType,self.effect))


    def getContext(self):
        if hasattr(self, "context"): return self.context
        lns =  self.effect.split("\n")
        if "lose this card" in lns[-1]: lns.pop()
        for i,ln in enumerate(lns):
            if ln[-1] not in ['.',':']:
                lns[i] += "."
        context = " ".join(lns)
        context = context.replace("=>", "→")
        context = context.replace("=^", "⤴")
        self.context = context
        return self.context

    def preview(self):
        ef = self.getContext()
        if self.type=="":
            return "{} : {}".format(self.name, ef)
        else:
            return "{} ({}): {}".format(self.name, self.type.lower(), ef)

    def preview_ls(self):
        ef = self.getContext()
        a = self.name
        if self.type!="": 
            b = " ({}) : ".format(self.type.lower())
        else:
            b = " : "
        return a,b,ef


    def __str__(self):
        return "<Status>({})".format(self.name)          


class Summon:
    def __init__(self, name, hp, trigger, lines, icon, typ="summon"):
        self.name = name
        if hp ==0: self.hp = "∞"
        else: self.hp = hp
        self.trigger = trigger
        self.lines = lines
        self.icon = icon
        self.iconPath = None
        self.type=typ
        self.path = "cards\\placard\\{}.png".format(self.name)
        G.SUMMONS[name] = self

    def setIconPath(self, rawIcon):
        self.iconPath = "assets\\icons\\{}\\{}.png".format(rawIcon.split(":")[0],rawIcon.split(":")[1])
        return self.iconPath

    def image(self):
        if self.iconPath==None: self.getIconPath()
        #return Image.open(self.iconPath).convert("RGBA")

    def print(self):
        print("{} ({})".format(self.name, self.hp))
        if self.iconPath==None: self.getIconPath()
        print(self.iconPath)
        for x in [self.trigger]+self.lines:
            print(x)
