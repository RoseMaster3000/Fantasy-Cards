from itertools import product

def prob(num_dice, target):
	success,total = 0,0
	for combination in product([1,2,3,4,5,6], repeat=num_dice):
		if sum(combination) >= target: success += 1
		total += 1

	return success/total

def reroll(dice, target):
	dice.sort()

	best_prob,best_dice = 0,0
	n = len(dice)
	for i in range(1,n+1):
		suffix = dice[-i:]
		new_prob = prob(n-i, target-sum(suffix))

		if best_prob < new_prob:
			best_prob = new_prob
			best_dice = n-i

	return best_dice, best_prob # reroll the lowest best_dice dice; probability of winning is best_prob