from PIL import Image, ImageFilter, ImageEnhance
import numpy as np
import colorsys


def reduceOpacity(im, opacity):
    assert opacity >= 0 and opacity <= 1
    if im.mode != 'RGBA':
        im = im.convert('RGBA')
    else:
        im = im.copy()
    alpha = im.split()[3]
    alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
    im.putalpha(alpha)
    return im


# takes PIL image, returns PIL image with hue rotated (0-360)
def colorize(image, hue):
	rgb_to_hsv = np.vectorize(colorsys.rgb_to_hsv)
	hsv_to_rgb = np.vectorize(colorsys.hsv_to_rgb)
	def shift_hue(arr, hout):
		r, g, b, a = np.rollaxis(arr, axis=-1)
		h, s, v = rgb_to_hsv(r, g, b)
		h = hout
		r, g, b = hsv_to_rgb(h, s, v)
		arr = np.dstack((r, g, b, a))
		return arr
	img = image.convert('RGBA')
	arr = np.array(np.asarray(img).astype('float'))
	new_img = Image.fromarray(shift_hue(arr, hue/360.).astype('uint8'), 'RGBA')
	return new_img


# Takes PIL image, returns PIL image with a soft shadow behind it
# Add a gaussian blur drop shadow to an image.  
def dropShadow( image, offset=(5,5), background=0xffffff, shadow=0x444444, border=8, iterations=3):
	"""
	image       - The image to overlay on top of the shadow.
	offset      - Offset of the shadow from the image as an (x,y) tuple. Can be positive or negative.
	background  - Background colour behind the image.
	shadow      - Shadow colour (darkness).
	border      - Width of the border around the image. This must be wide enough to account for the blurring of the shadow.
	iterations  - Number of times to apply the filter.  More iterations produce a more blurred shadow, but increase processing time.
	"""
	
	# Create the backdrop image -- a box in the background colour with a shadow on it.
	totalWidth = image.size[0] + abs(offset[0]) + 2*border
	totalHeight = image.size[1] + abs(offset[1]) + 2*border
	back = Image.new(image.mode, (totalWidth, totalHeight), background)
	
	# Place the shadow, taking into account the offset from the image
	shadowLeft = border + max(offset[0], 0)
	shadowTop = border + max(offset[1], 0)
	back.paste(shadow, [shadowLeft, shadowTop, shadowLeft + image.size[0], 
		shadowTop + image.size[1]] )
	
	# Apply the filter to blur the edges of the shadow.  Since a small kernel is used, the filter must be applied repeatedly to get a decent blur.
	n = 0
	while n < iterations:
		back = back.filter(ImageFilter.BLUR)
		n += 1
		
	# Paste the input image onto the shadow backdrop  
	imageLeft = border - min(offset[0], 0)
	imageTop = border - min(offset[1], 0)
	back.paste(image, (imageLeft, imageTop))
	
	return back
	

def resize(img, percX, percY=None):
	if percY == None:
		percY = percX
	new_width = int(img.width * percX/100)
	new_height = int(img.height * percY/100)
	img = img.resize( (new_width,new_height), Image.ANTIALIAS )
	return img

def batch_resize(image_dict, new_width):
	for ty in list(image_dict.keys()):
		new_height = int(image_dict[ty].height * (new_width/image_dict[ty].width))
		image_dict[ty] = image_dict[ty].resize( (new_width,new_height), Image.ANTIALIAS )
	return image_dict


def strokeText(draw, location, text, font, thickness):
	# prep data
	x,y = location
	t = thickness
	fillcolor = "white"
	shadowcolor = "black"
	font = NAME_FONT
	# stroke
	draw.text((x-t, y), text, font=font, fill=shadowcolor)
	draw.text((x+t, y), text, font=font, fill=shadowcolor)
	draw.text((x, y-t), text, font=font, fill=shadowcolor)
	draw.text((x, y+t), text, font=font, fill=shadowcolor)
	draw.text((x-t, y-t), text, font=font, fill=shadowcolor)
	draw.text((x+t, y-t), text, font=font, fill=shadowcolor)
	draw.text((x-t, y+t), text, font=font, fill=shadowcolor)
	draw.text((x+t, y+t), text, font=font, fill=shadowcolor)
	# body text
	draw.text((x, y), text, font=font, fill=fillcolor)
	return draw


if __name__ == "__main__":
	import sys

	image = Image.open(sys.argv[1])
	image.thumbnail( (200,200), Image.ANTIALIAS)

	dropShadow(image).show()
	dropShadow(image, background=0xeeeeee, shadow=0x444444, offset=(0,5)).show()
	 





