import G
import os, sys, threading
from windowFind import moveWindow
threads = []



def saveCard(res, outFolder, outFile, preview=False, typ="png"):
    def saveCardCore(res, outFolder, outFile, preview, typ):
        if preview: res.show()
        if not os.path.exists(outFolder):
            os.makedirs(outFolder)
        out = os.path.join(outFolder, outFile+"."+typ)
        res.save(out)
        print("\t{} Saved!".format(outFile))
        G.OUT_COUNT += 1
        return

    global threads
    if preview:
        t = threading.Thread(target=moveWindow, args=("JPEGView",3840,0,555,947))
        threads.append(t)
        t.start()
        res.show()
        return

    t = threading.Thread(target=saveCardCore, args=(res, outFolder, outFile, preview, typ))
    threads.append(t)
    t.start()





# The answer is "pyrsvg" - a Python binding for librsvg.
# There is an Ubuntu python-rsvg package providing it. Searching Google
# for its name is poor because its source code seems to be contained
# inside the "gnome-python-desktop" Gnome project GIT repository.

# import cairo
# import rsvg

# def svg2png(targetFile, outFile=None, xRes=512, yRes=512):
#     if outFile ==None:
#         outFile = "{}.png".format(targetFile.split(".")[0])

#     img = cairo.ImageSurface(cairo.FORMAT_ARGB32, xRes, yRes)
#     ctx = cairo.Context(img)
#     handle = rsvg.Handle(targetFile)
#     # for in memory SVG data:
#     # handle= rsvg.Handle(None, str(<svg data>))

#     handle.render_cairo(ctx)
#     img.write_to_png(outFile)




# alternate version using cairosvg
# from cairosvg import svg2png

# svg_code = """
#     <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
#         <circle cx="12" cy="12" r="10"/>
#         <line x1="12" y1="8" x2="12" y2="12"/>
#         <line x1="12" y1="16" x2="12" y2="16"/>
#     </svg>
# """
# svg2png(bytestring=svg_code,write_to='output.png')
