# libraries
from copy import copy
from PIL import Image, ImageDraw, ImageFont, ImageOps, ImageEnhance
from operator import itemgetter
from random import choice
from os import listdir
# globals
import G
from assets import *
# project files
from printer import *
from dataObject import *
from dataLoader import loadAll
from imageSave import saveCard
from imageEffects import *


def makeRect(draw, x,y,w,h, fill):
    draw.rectangle([(x,y), (x+w,y+h)], fill = fill)
    return draw, x+w,y+h

def textMove(draw, x, y, body, font, fill="#ffffff"):
    w, h = draw.textsize(body, font=font)
    draw.text((x,y), body, font=font, fill=fill)
    x += w
    y += h
    return draw, x, y

def textBox(draw, content, font, x,y, maxWidth, align="Left", fill="#ffffff", space=1.0, skip=False):
    _,hhh= draw.textsize("word", font=font)
    hhh = int(hhh*space)

    if skip!=False:
        fwx,_= draw.textsize(skip, font=font)
        content = content.replace(skip,"",1)

    align = align.upper()[0]
    content = content.replace("\n", " \n ")
    words = content.split(" ")
    buff = []

    for i,word in enumerate(words):
        buff.append(word)
        line = " ".join(buff)
        w, h = draw.textsize(line, font=font)
        if skip!=False: w += fwx
        # overflow lines
        if w > maxWidth or word=="\n":
            line = " ".join(buff[:-1])
            w, h = draw.textsize(line, font=font)
            if align=="L":
                if skip!=False:
                    draw.text( (x+fwx,y), line, font=font, fill=fill)
                    skip=False
                else: 
                    draw.text( (x,y), line, font=font, fill=fill)
            elif align=="R":
                draw.text( (x+(maxWidth-w),y), line, font=font, fill=fill)
            elif align=="C":
                draw.text( (x+maxWidth//2-w//2,y), line, font=font, fill=fill)
            if word=="\n":
                y+=int(hhh*1.5)
                buff= []
            else:
                y+=hhh
                buff = [buff[-1]]
    # last line
    line = " ".join(buff)
    w, h = draw.textsize(line, font=font)
    if align=="L":
        draw.text( (x,y), line, font=font, fill=fill)
    elif align=="R":
        draw.text( (x+maxWidth-w,y), line, font=font, fill=fill)
    elif align=="C":
        draw.text( (x+(maxWidth//2)-(w//2),y), line, font=font, fill=fill)
    y += hhh
    # return
    return draw, x,y


def makeMultiBar(res,draw, title,titleColor, counts, colors, xx,yy):
    # sanatize
    if type(colors)!=list: colors = [colors]
    if type(counts)!=list: counts = [counts]
    if len(colors)!=len(counts): raise Exception("[colors] and [counts] are different lengths")
    shadow = reduceOpacity(TEMPLATE["shadow"], 0.30)
    # write title
    w, h = draw.textsize(title, font=FONT["pdecks"])
    shad = shadow.resize( (w*2,h*2), Image.ANTIALIAS )
    res.paste(shad, (xx-int(w*1.5), yy-h//2), shad)
    draw.text( (xx-w-13,yy-7), title, font=FONT["pdecks"], fill=G.COLOR["black"])
    draw.text( (xx-w,yy), title, font=FONT["pdecks"], fill=titleColor)
    # prepare cursor
    ww = 1135//len(counts)
    hh = 63
    xx += 106
    yy += 82
    shadow = reduceOpacity(TEMPLATE["shadow"], 0.60)
    for i,num in enumerate(counts):
        # draw bar
        x = xx + (i*ww)
        y = yy
        draw, _,_ = makeRect(draw, x,y,   ww,hh, colors[i])

        # write count (w/ dropshadow)
        w, h = draw.textsize(counts[i], font=FONT["pcount"])
        x = x + (ww//2) - (w//2)
        y -= h//2
        shad = shadow.resize( (w*2,h*2), Image.ANTIALIAS )
        res.paste(shad, (x-w//2, y-h//2), shad)   
        draw.text( (x-10,y-7), counts[i], font=FONT["pcount"], fill=G.COLOR["black"])
        draw.text( (x,y), counts[i], font=FONT["pcount"], fill=G.COLOR["white"])
    return draw


def profTemplate(size, prof):
    bColor = prof.colors["dark"]
    fColor = prof.colors["neon"]
    art = Image.open(prof.portraitPath)
    # setup bg
    res = Image.new('RGB', size, color = bColor)
    art = resize(art, 107.1)
    res.paste(art, (0, 0), art)
    # draw border
    draw = ImageDraw.Draw(res)
    draw, _,_ = makeRect(draw, 0,3200, 2787, 1146, (0,0,0))
    draw, _,_ = makeRect(draw, 0,0,    60,4346, fColor)
    draw, _,_ = makeRect(draw, 2727,0, 60,4346, fColor)
    draw, _,_ = makeRect(draw, 0,0,    2787,60, fColor)
    draw, _,_ = makeRect(draw, 0,1703, 2787,60, fColor)
    draw, _,_ = makeRect(draw, 0,2512, 2787,60, fColor)
    draw, _,_ = makeRect(draw, 0,4286, 2787,60, fColor)

    borderMatte = TEMPLATE["border"].convert("RGBA")
    borderSolid = Image.new('RGBA', (2787, 4346), color=fColor)
    res.paste(borderSolid, (0,0), borderMatte)

    return res, draw



def makeProfession(prof, lvl=1, save=True):
    if type(prof)==str: prof = G.PROFESSIONS[prof.title()]
    if type(lvl)!=int: raise Exception("invalid lvl (int): "+str(lvl))
    if type(prof)!=Profession: raise Exception("invalid prof: "+str(prof))
    level = G.LEVELS[lvl]
    lvl -= 1
    ############################################
    # setup card template
    ############################################
    cardSize = (2787,4346) #59mm x 92mm (1200dpi)
    res, draw = profTemplate(cardSize, prof)
    ############################################
    # card header (lvl/name)
    ############################################
    w,h = draw.textsize(level, font=FONT["plevel"])
    ww,hh = draw.textsize(prof.name, font=FONT["pname"])
    spc = 70
    x = int((2787-w-ww-spc)/2)
    y = 120
    draw, _,_ = textMove(draw, x-20, y+90, level, font=FONT["plevel"], fill=(0,0,0,100))
    draw, x,_ = textMove(draw, x, y+110, level, font=FONT["plevel"], fill=G.COLOR["white"])
    x += spc
    draw, _,_ = textMove(draw, x-20, y-20, prof.name, font=FONT["pname"], fill=(0,0,0,100))
    draw, x,_ = textMove(draw, x, y, prof.name, font=FONT["pname"], fill=G.COLOR["white"])
    ############################################
    # card top (description/schools/icons)
    ############################################
    draw, _,_ = textBox(draw, prof.description, FONT["pdesc"], 140,500, 2456, "center", fill=prof.colors["light"])
    x = 1400
    y = 910 + (5-len(prof.schools))*75
    for scl in prof.schools:
        art = Image.open(scl.iconPath).convert("RGBA")
        art = art.resize( (160,160), Image.ANTIALIAS )
        res.paste(art, (x-5, y-5), art)
        draw.text( (x+200,y), scl.name+" ", font=FONT["pschools"], fill=G.COLOR["white"])
        y += 150
    ############################################
    # card middle (skill deck, health deck, power deck)
    ############################################
    draw = makeMultiBar(res,draw, "Skill Deck:", prof.colors["light"], prof.skillDecks[lvl], prof.colors["normal"], 1294,1810)
    draw = makeMultiBar(res,draw, "Health Deck:", prof.colors["light"], prof.healthDecks[lvl], ["#ff0000","#0000ff","#00ff00"], 1294,1810+200)
    powCnt, powCol = [],[]
    for i,p in enumerate(prof.powerDecks[lvl]):
        if int(p) != 0:
            powCnt.append(p)
            # powCol.append(G.COLOR[G.POWER_COLORS[i]])
            powCol.append(G.COLOR[G.POWER_COLORS[i]])
    draw = makeMultiBar(res,draw, "Power Deck:", prof.colors["light"], powCnt, powCol, 1294,1810+400)
    ############################################
    # card bottom (skill flow)
    ############################################
    w,h = draw.textsize(prof.flowName, font=FONT["flow"])
    draw,_,_ = textMove(draw, 2787//2-w//2+20, 2627-13, prof.flowName, font=FONT["flow"], fill=G.COLOR["black"])
    draw,_,_ = textMove(draw, 2787//2-w//2, 2627, prof.flowName, font=FONT["flow"], fill=G.COLOR["white"])

    rFlow = prof.renderFlow(lvl)
    if "draw HAND SIZE cards" in prof.flow[0] and prof.name!="Outlaw":
        draw,_,_ = textBox(draw, rFlow[0], FONT["ppflow"], 763,2900, 1200, "center", fill=G.COLOR["white"])
    else:
        draw,_,_ = textBox(draw, rFlow[0], FONT["ppflow"], 130,2900, 2467, "center", fill=G.COLOR["white"])

    x,y = 130, 2940+310
    if rFlow[3] == "": y+= 180

    for i in [1,2,3]:
        draw,_,yy = textBox(draw, rFlow[i], FONT["pflow"], x,y, 2467, "left", fill=prof.colors["neon"])
        for phrase in ["After", "Then", "If you do", "Finally"]:
            if rFlow[i].startswith(phrase):
                draw,_,_ = textBox(draw,  phrase+" ", FONT["pflow"], x,y, 2467, "left", fill=G.COLOR["white"])
        y = yy + 80

    saveCard(res, "cards/profession", "{}{}".format(prof.name, lvl+1), preview= not save)
    return




def makeSkill(skill, save=True):
    if type(skill)==str: skill = G.SKILLS[skill]
    if skill.scl=="Race Skills":
        makeRace(skill, save=True)
        return
    else:
        res = copy(TEMPLATE[skill.type+"_skill"])
        draw = ImageDraw.Draw(res)
    ############################################
    # card header (degree, name)
    ############################################
    shadow = reduceOpacity(TEMPLATE["shadow"], 0.90)
    w, h = draw.textsize(skill.name, font=FONT["stitle"])
    shad = shadow.resize( (int(w*2),int(h*1.8)), Image.ANTIALIAS )
    res.paste(shad, (2787//2-w, 160-h//2), shad)
    draw, _,_ = textBox(draw, skill.name, FONT["stitle"], 0,150, 2787, align="Center", fill=G.COLOR["white"])
    draw, _,_ = textBox(draw, G.STAR*skill.lvl, FONT["stars"], 0,55, 2787, align="Center", fill=G.COLOR["white"])
    ############################################
    # card top (lines)
    ############################################
    context = []
    x, y = 147, 750
    for ln in skill.lines:
        if ln == "──── or ────":
            draw,x,_ = textMove(draw, x, y, ln, font=FONT["line"], fill=G.COLOR["white"])
            x = 147
            y += 180  
            continue
        # create lns
        if "=>" in ln: arr = "=>"
        if "=^" in ln: arr = "=^"
        # populate context
        effect = ln.split(arr)[0]
        if "OR" in ln: effect = ln.split(arr)[0].split("OR")
        else: effect = [ln.split(arr)[0]]
        for e in effect:
            if "[" in e: e = e.split("[")[0]
            e = e.strip()
            if e not in context: context.append(e)
        # sanatize line
        ln = ln.replace(arr, G.AR[arr])
        ln = ln.replace("draw", "#")
        ln = ln.replace("range", "@")
        ln = ln.replace("( ", "(")
        ln = ln.replace(" )", ")")
        ln = ln.replace("(", "( ")
        ln = ln.replace(")", " )")   
        # draw line
        color = "yellow"
        for ci, char in enumerate(ln):
            if char == "#":
                x+=20
                ico = ICONS["custom"]["draw"].resize((120,120), Image.ANTIALIAS)
                res.paste(ico, (x,y+15), ico)
                x+=120
            elif char == "@":
                x-=12
                ico = ICONS["custom"]["target"].resize((130,130), Image.ANTIALIAS)
                res.paste(ico, (x,y+7), ico)
                x+=115

            elif char == "O" and ln[ci+1]=="R":
                oColor = color
                color = "white"
                draw,x,_ = textMove(draw, x, y, char, font=FONT["line"], fill=G.COLOR[color])
            elif char == "R" and ln[ci-1]=="O":
                draw,x,_ = textMove(draw, x, y, char, font=FONT["line"], fill=G.COLOR[color])
                color = oColor
                del oColor
            elif char in G.AR["=>"]:
                color = "white"
                draw,x,_ = textMove(draw, x, y-5, char, font=FONT["arrows"], fill=G.COLOR[color])
                color = "cyan"
            elif char in G.AR["=^"]:
                color = "white"
                draw,x,_ = textMove(draw, x, y, char, font=FONT["arrows"], fill=G.COLOR[color])
                color = "cyan"
            else:
                draw,x,_ = textMove(draw, x, y, char, font=FONT["line"], fill=G.COLOR[color])
        x = 147
        y += 180
    ############################################
    # card middle (summon, context)
    ############################################
    summon,skillBuff,skillDebuff=[],[],[]
    for i in context:
        if "Summon" in i:
            summon.append(i)
        elif i in G.STATUSES and G.STATUSES[i].type=="Skill Buff":
            skillBuff.append(i)
        elif i in G.STATUSES and G.STATUSES[i].type=="Skill Debuff":
            skillDebuff.append(i)           

    # SUMMON SCRAP
    if len(summon):
        context.remove(summon[0])
        res.paste(TEMPLATE["scrap"], (0,0), TEMPLATE["scrap"])
        # parse summon
        summon = summon[0].split("Summon")[1].strip()
        if summon[0].isdigit():
            summon = summon.split(" ", 1)[1]
            if summon.endswith("es"): summon = summon[0:-2]
            elif summon.endswith("s"): summon = summon[0:-1]
        summon_obj = G.SUMMONS[summon]
        internalLines = summon_obj.lines
        summon = [summon]
        # trigger line
        x,y = 468,1180
        color = {"martial":"#940000","magical":"#0034ad"}[skill.type]
        draw, _,y = textBox(draw, summon_obj.trigger, FONT["sline"], 0,y, 2797, align="Center", fill=color)
        # hp dot
        if int(summon_obj.hp)!=0:
            context += ["Summon"]
            xx,yy = 2170, 1170
            hp_window = TEMPLATE["dot_{}".format(skill.type)]
            res.paste(hp_window, (xx,yy), hp_window)
            draw, _,_ = textBox(draw, "EP", FONT["sline"], xx,yy+70, 360, align="Center", fill="#e5d8be")
            draw, _,_ = textBox(draw, summon_obj.hp, FONT["bline"], xx,yy+150, 360, align="Center", fill="#e5d8be")
        else:
            context += ["Immortal Summon"]

    # SKILL BUFF SCRAP
    if len(skillBuff):
        context.remove(skillBuff[0])
        #context += ["Skill Buff"]
        res.paste(TEMPLATE["scrap"], (0,0), TEMPLATE["scrap"])
        # parse skill status
        internalLines = G.STATUSES[skillBuff[0]].effect.split("\n")
        # scrap header
        x,y = 468,1195
        color = {"martial":"#940000","magical":"#0034ad"}[skill.type]
        trigger = "Target's new race skill, {}:".format(skillBuff[0].upper())
        draw, _,y = textBox(draw, trigger, FONT["sline"], 0,y, 2797, align="Center", fill=color)

    # SKILL DEBUFF SCRAP
    if len(skillDebuff):
        context.remove(skillDebuff[0])
        context += ["Skill Debuff"]
        res.paste(TEMPLATE["scrap"], (0,0), TEMPLATE["scrap"])
        # parse skill status
        internalLines = G.STATUSES[skillDebuff[0]].effect.split("\n")
        # scrap header
        x,y = 468,1195
        color = {"martial":"#940000","magical":"#0034ad"}[skill.type]
        trigger = "Target's next action, {}:".format(skillDebuff[0].upper())
        draw, _,y = textBox(draw, trigger, FONT["sline"], 0,y, 2797, align="Center", fill=color)

    # SCRAP LINES
    if len(summon+skillBuff+skillDebuff):
        x-=50
        y+=10
        y+= 60*(4-len(internalLines))
        for ln in internalLines:
            # create lns
            if "=>" in ln: arr = "=>"
            if "=^" in ln: arr = "=^"
            # populate context
            effect = ln.split(arr)[0]
            if "[" in effect: effect = effect.split("[")[0]
            effect = effect.strip()
            if effect not in context: context.append(effect)
            # sanatize line
            ln = ln.replace(arr, G.AR[arr])
            ln = ln.replace("draw", "#")
            ln = ln.replace("range", "@")
            ln = ln.replace("( ", "(")
            ln = ln.replace(" )", ")")
            ln = ln.replace("(", "( ")
            ln = ln.replace(")", " )")   
            # draw line
            color = "black"
            for ci, char in enumerate(ln):
                if char == "#":
                    x+=20
                    ico = ICONS["custom"]["draw_b"].resize((120,120), Image.ANTIALIAS)
                    res.paste(ico, (x,y+15), ico)
                    x+=120
                elif char == "@":
                    x-=12
                    ico = ICONS["custom"]["target_b"].resize((130,130), Image.ANTIALIAS)
                    res.paste(ico, (x,y+7), ico)
                    x+=115
                elif char in G.AR["=>"]:
                    draw,x,_ = textMove(draw, x, y-5, char, font=FONT["arrows"], fill=G.COLOR[color])
                elif char in G.AR["=^"]:
                    draw,x,_ = textMove(draw, x, y, char, font=FONT["arrows"], fill=G.COLOR[color])
                else:
                    draw,x,_ = textMove(draw, x, y, char, font=FONT["line"], fill=G.COLOR[color])
            x = 468-50
            y += 120

    # RELEVANT RULES (CONTEXT)
    context = [G.STATUSES[i] for i in context if i in G.STATUSES]
    x,y = 648,2230
    contextLen = 0
    for sts in context:
        a,b,c = sts.preview_ls()
        draw, _,_ = textBox(draw, a, FONT["context"], x,y, 2020, fill=G.COLOR["yellow"], space=1.2)
        draw, _,yy = textBox(draw, a+b+c, FONT["context"], x,y, 2020, fill=G.COLOR["white"], space=1.2, skip=a)
        contextLen += yy-y+45
        y = yy + 45

    ############################################
    # card bottom (school-icon, flavor, serial-number)
    ############################################
    if contextLen < 1080:
        flavor = skill.flavor.replace("~"," -- ")
        res.paste(TEMPLATE[skill.type+"_scroll"], (0,0), TEMPLATE[skill.type+"_scroll"])
        # get flavor height
        test=Image.new('L', (1507,500))
        testd=ImageDraw.Draw(test)
        testd,_,length = textBox(testd, flavor, FONT["sdesc"], 0,0, 1507, fill=G.COLOR["black"], space=1.0)
        del test; del testd;
        # write flavor
        draw, _,_ = textBox(draw, flavor, FONT["sdesc"], 854,3380+(650-length)//2, 1540, fill=G.COLOR["black"], space=1.0)

    draw, _,_ = textBox(draw, skill.serial, FONT["serial"], 247,4170, 200, align="Center", fill=G.COLOR["white"])
    sclIcon = ICONS["schools"][skill.school.name.lower()].resize( (320,320), Image.ANTIALIAS )
    res.paste(sclIcon, (180,2300), sclIcon)
    ############################################
    # school banner (rotated)
    ############################################
    txt=Image.new('L', (1530,500))
    d = ImageDraw.Draw(txt)
    ftx,_ = draw.textsize(skill.school.name, font=FONT["sschool"])
    d.text( (1530-ftx, 0), skill.school.name,  font=FONT["sschool"], fill=G.COLOR["white"])
    w=txt.rotate(90,  expand=1)
    res.paste( ImageOps.colorize(w, (0,0,0), (255,255,255)), (200,2550+(1500-ftx)//2),  w)

    #res.paste(TEMPLATE["border"], (0, 0), TEMPLATE["border"])
    saveCard(res, "cards/skill", skill.name, preview=not save)




def makeRace(skill, save=True):
    if type(skill)==str: skill = G.SKILLS[skill]
    res = copy(RACES[skill.name.lower()])
    draw = ImageDraw.Draw(res)
    ############################################
    # card header (degree, name)
    ############################################
    draw, _,_ = textBox(draw, skill.name, FONT["rtitle"], 0-15,2300-15, 2787 , align="Center", fill=G.COLOR["black"])
    draw, _,_ = textBox(draw, skill.name, FONT["rtitle"], 0,2300, 2787 , align="Center", fill=G.COLOR["white"])

    ############################################
    # card top (lines)
    ############################################
    context = []
    x,y = 147,2912
    for ln in skill.lines:
        if ln == "──── or ────":
            draw,x,_ = textMove(draw, x, y, ln, font=FONT["line"], fill=G.COLOR["white"])
            x = 147
            y += 180  
            continue
        # create lns
        if "=>" in ln: arr = "=>"
        if "=^" in ln: arr = "=^"
        # populate context
        effect = ln.split(arr)[0]
        if "OR" in ln: effect = ln.split(arr)[0].split("OR")
        else: effect = [ln.split(arr)[0]]
        for e in effect:
            if "[" in e: e = e.split("[")[0]
            e = e.strip()
            if e not in context: context.append(e)
        # sanatize line
        ln = ln.replace(arr, G.AR[arr])
        ln = ln.replace("draw", "#")
        ln = ln.replace("range", "@")
        ln = ln.replace("( ", "(")
        ln = ln.replace(" )", ")")
        ln = ln.replace("(", "( ")
        ln = ln.replace(")", " )")   
        # draw line
        color = "yellow"
        for ci, char in enumerate(ln):
            if char == "#":
                x+=20
                ico = ICONS["custom"]["draw"].resize((120,120), Image.ANTIALIAS)
                res.paste(ico, (x,y+15), ico)
                x+=120
            elif char == "@":
                x-=12
                ico = ICONS["custom"]["target"].resize((130,130), Image.ANTIALIAS)
                res.paste(ico, (x,y+7), ico)
                x+=115

            elif char == "O" and ln[ci+1]=="R":
                oColor = color
                color = "white"
                draw,x,_ = textMove(draw, x, y, char, font=FONT["line"], fill=G.COLOR[color])
            elif char == "R" and ln[ci-1]=="O":
                draw,x,_ = textMove(draw, x, y, char, font=FONT["line"], fill=G.COLOR[color])
                color = oColor
                del oColor

            elif char in G.AR["=>"]:
                color = "white"
                draw,x,_ = textMove(draw, x, y-5, char, font=FONT["arrows"], fill=G.COLOR[color])
                color = "cyan"
            elif char in G.AR["=^"]:
                color = "white"
                draw,x,_ = textMove(draw, x, y, "⬏", font=FONT["arrows"], fill=G.COLOR[color])
                color = "cyan"
            else:
                draw,x,_ = textMove(draw, x, y, char, font=FONT["line"], fill=G.COLOR[color])
        x = 147
        y += 180
    ############################################
    # card bottom (context, serial number)
    ############################################
    # break line
    y += 100
    temp = Image.new('RGBA', (2400, 10), color=G.COLOR["white"])
    res.paste(temp, ((2787-2400)//2,y), temp)
    y += 150
    # flavor text
    draw, _,y = textBox(draw, skill.flavor, FONT["sumdesc"], 787//2,y, 2000, align="Center", fill="#a9a9a9", space=1.1)
    # serial number
    # draw, _,_ = textBox(draw, skill.serial, FONT["serial"], 247,4170, 200, align="Center", fill=G.COLOR["white"])

    saveCard(res, "cards/skill", skill.name, preview=not save)


def project_width(line):
    pw=0
    if "draw" in line: pw+=120
    if "range" in line: pw+=130
    if "=>" in line: pw+=130
    if "=^" in line: pw+=130
    line = line.replace("draw","").replace("range","").replace("=>","").replace("=^","")
    res = copy(TEMPLATE["status"])
    draw = ImageDraw.Draw(res)
    x,_ = draw.textsize(line, font=FONT["line"])
    print(line)
    print(x)
    del draw
    del res
    pw+=x
    return pw

def makeStatus(status, save=True):
    if type(status)==str: status = G.STATUSES[status]
    #res = colorize( TEMPLATE["status"] , G.HUES["yellow"])
    res = copy(TEMPLATE["status"])
    draw = ImageDraw.Draw(res)

    # header
    shadow = reduceOpacity(TEMPLATE["shadow"], 0.40)
    w, h = draw.textsize(status.name, font=FONT["ctitle"])
    shad = shadow.resize( (int(w*2),int(h*1.8)), Image.ANTIALIAS )
    res.paste(shad, (2787//2-w, 250-h//2), shad)
    draw, _,_ = textBox(draw, status.name, FONT["ctitle"], 0,250, 2787, align="Center", fill=G.COLOR["white"])

    # middle icon
    if "Treat this like a power card worth" in status.effect:
        amt = status.effect.split("\n")[0].split(" ")[-1][:-1]
        draw, _,_ =  textBox(draw, amt, FONT["stsPow"], 0-20,1280-20, 2787, align="Center", fill=G.COLOR["black"])
        draw, _,_ =  textBox(draw, amt, FONT["stsPow"], 0,1280, 2787, align="Center", fill=G.COLOR["white"])
    else:
        ico = Image.open(status.icon).convert("RGBA").resize((650,650), Image.ANTIALIAS)
        temp = Image.new('RGBA', (650, 650), color=G.COLOR["white"])
        res.paste(temp, (2787//2-650//2, 1324), ico)



    # body
    draw, _,_ = textBox(draw, " "+status.type.lower()+" ", FONT["ctype"], 0,2500, 2787, align="Center", fill=G.COLOR["white"])
    x,y = 219, 2888
    context = []
    for ln in status.effect.split("\n"):
        if "[" not in ln:
            x=219
            y+=50
            # enscribe status text
            draw, _,_ = textBox(draw, ln, FONT["cdesc"], x-10,y-10, 2300, align="Center", fill=G.COLOR["black"], space=1.1)
            draw, _,y = textBox(draw, ln, FONT["cdesc"], x,y, 2300, align="Center", fill=G.COLOR["white"], space=1.1)
            y+=70
        else:
            x=(2787-project_width(ln))//2
            # create lns
            if "=>" in ln: arr = "=>"
            if "=^" in ln: arr = "=^"
            # sanatize line
            ln = ln.replace(arr, G.AR[arr])
            ln = ln.replace("draw", "#")
            ln = ln.replace("range", "@")
            ln = ln.replace("( ", "(")
            ln = ln.replace(" )", ")")
            ln = ln.replace("(", "( ")
            ln = ln.replace(")", " )")
            # populate context
            effect = ln.split(arr)[0]
            if "OR" in ln: effect = ln.split(arr)[0].split("OR")
            else: effect = [ln.split(arr)[0]]
            for e in effect:
                if "[" in e: e = e.split("[")[0]
                e = e.strip()
                if e not in context: context.append(e)
            # draw line
            color = "yellow"
            for ci, char in enumerate(ln):
                if char == "#":
                    x+=20
                    ico = ICONS["custom"]["draw"].resize((120,120), Image.ANTIALIAS)
                    res.paste(ico, (x,y+15), ico)
                    x+=120
                elif char == "@":
                    x-=12
                    ico = ICONS["custom"]["target"].resize((130,130), Image.ANTIALIAS)
                    res.paste(ico, (x,y+7), ico)
                    x+=115
                elif char in G.AR["=>"]:
                    draw,x,_ = textMove(draw, x, y-5, char, font=FONT["arrows"], fill=G.COLOR["white"])
                    color = "cyan"
                elif char in G.AR["=^"]:
                    draw,x,_ = textMove(draw, x, y, char, font=FONT["arrows"], fill=G.COLOR["white"])
                    color = "cyan"
                else:
                    draw,x,_ = textMove(draw, x, y, char, font=FONT["line"], fill=G.COLOR[color])
            x = 450
            y += 140


    # break line
    if "Skill" in status.type:
        context = [G.STATUSES[i] for i in context if i in G.STATUSES]
        contextLen = 0
        if len(context)!=0:
            y += 80
            temp = Image.new('RGBA', (2387, 10), color=G.COLOR["white"])
            res.paste(temp, (200,y), temp)
            y += 80
        # context
        for sts in context:
            draw, _,yy = textBox(draw, sts.preview(), FONT["context"], x,y, 2020, fill=G.COLOR["white"], space=1.1, skip=sts.name)
            draw, _,_ = textBox(draw, sts.name, FONT["context"], x,y, 2020, fill=G.COLOR["yellow"], space=1.1)
            contextLen += yy-y+45
            y = yy + 45


    # save images
    saveCard(res, "cards/status", status.name, preview=not save)

    if "obstacle" in status.type.lower():
        tok = Image.open("assets\\templates\\token.png").convert("RGBA")
        ico = Image.open(status.icon).convert("RGBA").resize((730,730), Image.ANTIALIAS)
        tok.paste(ico, (235, 235), ico)
        saveCard(tok, "cards/token", status.name, preview=not save)


def makePlacard(summon, save=True):
    if type(summon)==str: summon = G.SUMMONS[summon]
    if int(summon.hp)==0:
        res = copy(TEMPLATE["summon_immortal"])
    else:
        res = copy(TEMPLATE["summon"])
    draw = ImageDraw.Draw(res)
    color = "#000000"
    # Summon Name

    draw, _,_ = textBox(draw, summon.name, FONT["sumtitle"], 650,170, 2070, align="Center", fill=G.COLOR["black"])
    draw, _,_ = textBox(draw, summon.name, FONT["sumtitle"], 650+20,170+20, 2070, align="Center", fill=G.COLOR["white"])
    # Summon Icon
    x,y = 238, 202
    summonIcon = Image.open(summon.iconPath).convert("RGBA").resize((340,340), Image.ANTIALIAS)
    temp = Image.new('RGBA', (340, 340), color=G.COLOR["white"])
    res.paste(temp, (x,y), summonIcon)
    # Trigger Text
    x,y = 330,780
    draw, _,y = textBox(draw, summon.trigger, FONT["context"], x,y, 2787, align="Left", fill=color)
    # HP Scrap
    if int(summon.hp)!=0:
        draw, _,_ = textBox(draw, "EP: "+summon.hp, FONT["sumhp"], 2245,1860, 1000, align="Left", fill=color)

    # Lines
    y+= 60
    y+= (4-len(summon.lines))*60
    x+=70
    xx=x
    for ln in summon.lines:
        # create lns
        if "=>" in ln: arr = "=>"
        if "=^" in ln: arr = "=^"
        # sanatize line
        ln = ln.replace(arr, G.AR[arr])
        ln = ln.replace("draw", "#")
        ln = ln.replace("range", "@")
        ln = ln.replace("( ", "(")
        ln = ln.replace(" )", ")")
        ln = ln.replace("(", "( ")
        ln = ln.replace(")", " )")   
        # draw line
        color = "black"
        for ci, char in enumerate(ln):
            if char == "#":
                x+=20
                ico = ICONS["custom"]["draw"].resize((120,120), Image.ANTIALIAS)
                res.paste(ico, (x,y+15), ico)
                x+=120
            elif char == "@":
                x-=12
                ico = ICONS["custom"]["target_b"].resize((130,130), Image.ANTIALIAS)
                res.paste(ico, (x,y+7), ico)
                x+=115
            elif char in G.AR["=>"]:
                draw,x,_ = textMove(draw, x, y-5, char, font=FONT["arrows"], fill=G.COLOR[color])
            elif char in G.AR["=^"]:
                draw,x,_ = textMove(draw, x, y, char, font=FONT["arrows"], fill=G.COLOR[color])
            else:
                draw,x,_ = textMove(draw, x, y, char, font=FONT["line"], fill=G.COLOR[color])
        x = xx
        y += 120



    saveCard(res, "cards/placard", summon.name, preview=not save)




def makeKind(megaDict, makeFunc, what, lvl=None):
    existAlready = listdir("cards\\{}".format(what))
    amountTotal = len(megaDict)
    print("Making {} {}s...".format(amountTotal, what))

    for i,thing in enumerate(megaDict):
        thing = megaDict[thing]
        if thing.name+".png" not in existAlready:
            printf(">>"+thing.name)
            printProgressBar(i,amountTotal)
            if what=="profession": makeFunc(thing, lvl=lvl, save=True)
            else: makeFunc(thing, save=True)



if __name__ == "__main__":
    loadAll(refreshAll=False)

    #makeSkill("Feline", save=False)
    # makeSkill("Boiling Anger", save=true)
    #makePlacard("Bear", save=False)
    #makeStatus("Boost", save=False)
    #makeRace("Pescan", save=False)
    #makeProfession("Spellhand", 1, save=False)
    # raise Exception("STOP")


    makeKind(G.SUMMONS, makePlacard, "placard")
    makeKind(G.STATUSES, makeStatus, "status")
    makeKind(G.SKILLS, makeSkill, "skill")
    makeKind(G.PROFESSIONS, makeProfession, "profession", lvl=1)


