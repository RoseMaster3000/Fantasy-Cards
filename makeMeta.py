import G
from dataLoader import loadAll

loadAll(refreshAll=False)
# for skill in G.SKILLS:
# for summon in G.SUMMONS:
# for prof in G.PROFESSIONS:
lines = ["name,deck"]
for s in G.STATUSES:
	status = G.STATUSES[s]
	n = status.name
	t = status.type.split(" ")[0]
	if t in ["Edge","Node",""]: t="None"
	lines.append("{},{}".format(n, t))

with open("sets\\meta.csv", "w") as f:
	f.write("\n".join(lines))
