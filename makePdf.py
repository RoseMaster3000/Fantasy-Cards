# libraries
import pickle
from copy import copy
from PIL import Image, ImageDraw, ImageFont, ImageOps, ImageEnhance
from operator import itemgetter
from random import choice
from os import listdir
import os
from fpdf import FPDF
from shutil import copyfile
from collections import defaultdict
# globals
import G
from assets import *
from makeSet import *
# project files
from printer import *
from dataObject import *
from dataLoader import loadAll
from imageSave import saveCard
from imageEffects import *



# durn dict to list
def generateList(*args):
	outList = []
	for aDict in args:
		for key in aDict:
			for i in range(aDict[key]):
				outList.append(key)
	return outList


def getSetNum():
	setNum = 1
	files = os.listdir("sets")
	for f in files:
		if f.startswith("set") and f.endswith(".pdf"):
			setNum = max(int(f.split("set")[1].split(".")[0]), setNum)
	return setNum+1


def fullCards(cardDict, setNum):
	# turn cardDict -> quantitiyDict (key is the quant, value is list of paths)

	# make pdf for every quantity
	# for q in quantityDict:

	pdf = FPDF('P','mm',(59+6,92+6))
	pdf.add_page()

	h = "#ceb8a9"
	rgb = tuple(int(h.lstrip("#")[i:i+2], 16) for i in (0, 2, 4))


	pdf.set_fill_color(*rgb)
	pdf.rect(x=0, y=0, w=59+6, h=92+6, style = 'F')


	printf("SAVING PDF...",end="")

	folder = "sets\\set{}".format(setNum)
	if not os.path.exists(folder):
		os.mkdir(folder)
	pdf.output("{}\\test.pdf".format(folder), "F")
	printf("DONE!")



def addCards(pdf, cardDict, cardback=None):
	cardList = generateList(cardDict)
	w = 59
	h = 92

	print(cardback)
	for i,card in enumerate(cardList):
		printProgressBar(i,len(cardList))
		if i%8 == 0:
			# add card backs
			# ...
			# prep next page
			x = 21
			y = 15
			pdf.add_page()
			pdf.line(0, 15, 400, 15)
			pdf.line(0, 15+92, 400, 15+92)
			pdf.line(0, 15+92+92, 400, 15+92+92)
		if x > 270-w:
			pdf.line(x, y-30, x, y+200)
			x = 21
			y += 92
		pdf.image(card,x,y,w,h)
		pdf.line(x, y-30, x, y+200)
		x+=59

	return pdf


def addTokens(pdf, tokenDict):
	tokenList = generateList(tokenDict)
	w = 25
	h = 25
	for i,card in enumerate(tokenList):
		printProgressBar(i,len(tokenList))
		if i%63 == 0:
			# add card backs
			# ???
			# prep next page
			x = 21
			y = 15
			pdf.add_page()
			pdf.line(0, 15, 400, 15)
			pdf.line(0, 15+h, 400, 15+h)
			pdf.line(0, 15+h+h, 400, 15+h+h)
		if x > 270-w:
			pdf.line(x, y-30, x, y+200)
			x = 21
			y += h
		pdf.image(card,x,y,w,h)
		pdf.line(x, y-30, x, y+200)
		x+=w
	return pdf




def printCardReport():
	def rev(d):
		c = 0
		for k in d:
			if d[k] == 0: continue
			c += d[k]
			print("{} : {}".format(k, d[k]))
		#input("...")
		return c

	count = 0
	# core
	# count += rev(powerDict)
	# count += rev(healthDict)
	count += rev(skillDict) * 4
	# statuses
	count += rev(pStatusDict)
	count += rev(hStatusDict)
	# obstacles
	count += rev(oStatusDict)
	count += rev(tokenDict)
	print("TOTAL:", count)


###############################################################
#   MAIN
###############################################################


if __name__ == "__main__":
	### init
	release_prof = ["Soldier","Outlaw","Guardian","Engineer","Performer","Priest","Scholar","Shaman","Spellhand"]
	loadAll(refreshAll=False, verbose=True)
	setNum = getSetNum()

	### get counts (discrete dicts)
	excessive = True
	profDict = profSet(release_prof)
	talentDict = talentSet()
	powerDict = powerSet(excessive=excessive)
	healthDict = healthSet(excessive=excessive)
	skillDict = skillSet(release_prof, excessive=excessive)
	pStatusDict, hStatusDict, sStatusDict, oStatusDict, tokenDict, summonDict = statusSet(release_prof)


	### isolate/filter changed features
	if False:
		changedStatus = [
		]
		changedSkills = [
		]
		changedSchools = [
		]
		changedProfessions = [
		]
		### filter out media that had not changed
		skillDict =  filterSkills(
			skillDict,
			changedStatus,
			changedSkills,
			changedSchools
		)
		pStatusDict,hStatusDict,sStatusDict,oStatusDict = filterStatus(
			pStatusDict,
			hStatusDict,
			sStatusDict,
			oStatusDict,
			changedStatus
		)
		profDict = profSet(changedProfessions)


	### Preview Inventory Counts
	if True:
		fullDict = {
			**sStatusDict,
			**pStatusDict,
			**hStatusDict,
			**oStatusDict,

			**skillDict,
			**powerDict,
			**healthDict,

			**profDict,
			**talentDict,
			#**tokenDict,
			**summonDict
		}

		#with open("welcome.txt") as file:


		printf(end="---")
		d_view = [ (v,k) for k,v in fullDict.items() ]
		d_view.sort(reverse=True) # natively sort tuples by first element
		total=0
		for v,k in d_view:
			print("{}\t{}".format(k,v))
			total+=v
		
		printf(end="---")
		print("TOT:",total)
		print("VAR:", len(list(fullDict.keys())))
		printf(end="---")
		print("SKILLS:", len(list(skillDict.keys())))
		input()

	### load PDF
	if False:
		pdf = FPDF(orientation = 'L', unit = 'mm', format='Letter')
		fullDict = {
			#**sStatusDict,
			#**pStatusDict,
			#**hStatusDict,
			#**oStatusDict,

			**skillDict
			#**summonDict,
			#**profDict
		}

		pdf = addCards(pdf, fullDict)
		printf("SAVING PDF...",end="")
		pdf.output("sets\\set{}.pdf".format(setNum), "F")
		printf("DONE!")


	### make set folder opf pdfs
	if False:
		fullDict = {"cards\\health\\Flesh.png": 30}
		fullCards(fullDict, setNum)
	# pdf = FPDF(orientation = 'L', unit = 'mm', format='Letter')
	# pdf = addCards(pdf, powerDict, "cards\\back\\power.jpg")
	# pdf = addCards(pdf, pStatusDict, "cards\\back\\power.jpg")
	# pdf = addCards(pdf, healthDict, "cards\\back\\health.jpg")
	# pdf = addCards(pdf, hStatusDict, "cards\\back\\health.jpg")
	# pdf = addCards(pdf, skillDict, "cards\\back\\skill.jpg")
	# pdf = addCards(pdf, sStatusDict, "cards\\back\\skill.jpg")

	# pdf = addCards(pdf, profDict, "cards\\back\\profession.jpg")
	# pdf = addCards(pdf, oStatusDict, "cards\\back\\obstacle.jpg")
	# pdf = addTokens(pdf, tokenDict)

	# printf("SAVING PDF...",end="")
	# pdf.output("sets\\set{}.pdf".format(setNum), "F")
	# printf("DONE!")





