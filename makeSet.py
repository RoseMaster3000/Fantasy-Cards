# libraries
import pickle
from copy import copy
from PIL import Image, ImageDraw, ImageFont, ImageOps, ImageEnhance
from operator import itemgetter
from random import choice
from os import listdir
import os
from fpdf import FPDF
from shutil import copyfile
from collections import defaultdict
# globals
import G
from assets import *
# project files
from printer import *
from dataObject import *
from dataLoader import loadAll
from imageSave import saveCard
from imageEffects import *


###############################################################
#   CALC CARD COUNTS
###############################################################


def healthSet(excessive=False):
	healthCards = [0,0,0]

	## get Ether Count
	etherCards = 0
	for smn in G.SUMMONS.values(): etherCards = max(etherCards, int(smn.hp))
	etherCards *= 6

	### every deck built at once HEALTH count 
	if excessive:
		for prf in G.PROFESSIONS.values():
			for i,hlt in enumerate(prf.healthDecks[0]):
				healthCards[i] += int(hlt)
		return {
			"cards\\health\\Flesh.png": healthCards[0],
			"cards\\health\\Willpower.png": healthCards[1],
			"cards\\health\\Fortitude.png": healthCards[2],
			"cards\\health\\Ether.png": etherCards
		}

	### worst case scenario POWER count 
	else:
		for lvl in range(5):
			decks = []
			for prf in G.PROFESSIONS.values():
				decks.append(prf.healthDecks[lvl])
			for i in range(3):
				maxList = sorted(decks, key=lambda x: x[i])[-4:]
				tmp = sum([int(deck[i]) for deck in maxList])
				healthCards[i] = max(healthCards[i], tmp)
		return {
			"cards\\health\\Flesh.png": healthCards[0]*3,
			"cards\\health\\Willpower.png": healthCards[1]*2,
			"cards\\health\\Fortitude.png": healthCards[2]*2,
			"cards\\health\\Ether.png": etherCards
		}


def powerSet(excessive=False):
	powerCount = len(G.PROFESSIONS["Soldier"].powerDecks[0])
	powerCards = [0]*powerCount

	### every deck built at once POWER count 
	if excessive:
		for prf in G.PROFESSIONS.values():
			for i,pwr in enumerate(prf.powerDecks[0]):
				powerCards[i] += int(pwr)
	### worst case scenario POWER count
	else:
		for lvl in range(5):
			decks = []
			for prf in G.PROFESSIONS.values():
				decks.append(prf.powerDecks[lvl])
			for i in range(powerCount):
				maxList = sorted(decks, key=lambda x: x[i])[-4:]
				tmp = sum([int(deck[i]) for deck in maxList])
				powerCards[i] = max(powerCards[i], tmp+1)

	### massage output to dict
	powerCards = [0]+powerCards
	powerDict = {}
	for i,pc in enumerate(powerCards):
		pwrPath = "cards\\power\\{}.png".format(i)
		powerDict[pwrPath] = pc + 1
	return powerDict


def skillSet(profs_in_release=None, excessive=False):
	if profs_in_release==None: profs_in_release=list(G.PROFESSIONS)
	sclOccur = {}
	for scl in G.SCHOOLS: sclOccur[scl]=0 # 

	### every deck built at once SCHOOL count
	if excessive:
		for prf in G.PROFESSIONS.values():
			for scl in prf.schools:
				sclOccur[scl.name]+=1
	### worst case scenario SCHOOL count 
	else:
		for prf in G.PROFESSIONS.values():
			if prf.name in profs_in_release:
				for scl in prf.schools:
					sclOccur[scl.name]+=1
		### double sclOccur X2 (for enemies)
		for key in sclOccur:
			sclOccur[key] *= 2

	### turn school occurance into skillDict
	skillDict = {}
	for scl in G.SCHOOLS:
		for skl in G.SCHOOLS[scl].skills:
			skillDict[skl.cardPath] = sclOccur[scl]
			if scl == "Race Skills":
				skillDict[skl.cardPath]+=1
			elif scl == "Enemy Skills":
				skillDict[skl.cardPath]+=2
	return skillDict


def statusSet(profs_in_release):
	pStatusDict = defaultdict(lambda: 0)
	hStatusDict = defaultdict(lambda: 0)
	oStatusDict = defaultdict(lambda: 0)
	sStatusDict = defaultdict(lambda: 0)
	tokenDict = defaultdict(lambda: 0)
	summonDict = defaultdict(lambda: 0)

	for s in {**G.STATUSES, **G.SUMMONS}: 
		sCount = []

		# get status/summon counts for each profession
		for prf in profs_in_release:
			prf = G.PROFESSIONS[prf]
			maxS = 0
			for scl in prf.schools:
				for skl in scl.skills: 
					dic = skl.inflictDict() # gets statuses/summons in skill
					if s in dic:
						maxS = max(maxS, dic[s])
			sCount.append(maxS)

		# summate top 4 profession status/summon counts
		if "summon" and s in G.SUMMONS:
			summonDict[G.SUMMONS[s].path] = sum(sorted(sCount)[-4:]) * 2

		elif "obstacle" in G.STATUSES[s].type.lower():
			oStatusDict[G.STATUSES[s].cardPath] = 1
			tokenDict[G.STATUSES[s].tokenPath] = sum(sorted(sCount)[-4:]) * 2 * 3

		elif "health" in G.STATUSES[s].type.lower():
			hStatusDict[G.STATUSES[s].cardPath] = sum(sorted(sCount)[-4:]) * 2

		elif "power" in G.STATUSES[s].type.lower():
			pStatusDict[G.STATUSES[s].cardPath] = sum(sorted(sCount)[-4:]) * 2

		elif "skill" in G.STATUSES[s].type.lower():
			sStatusDict[G.STATUSES[s].cardPath] = sum(sorted(sCount)[-4:]) * 2



	# add some BOOST for sorcerer skill flow
	pStatusDict["cards\\status\\Boost.png"] += 4
	# add some Large Objects for DM
	tokenDict["cards\\token\\Large Object.png"] += 5
	# return
	return pStatusDict, hStatusDict, sStatusDict, oStatusDict, tokenDict, summonDict


def profSet(release_prof, copyCnt=1):
	profs = listdir("cards\\profession")
	outDict = {}
	for p in profs:
		for r in release_prof:
			if r in p:
				outDict["cards\\profession\\"+p]=copyCnt
	return outDict




def talentSet(copyCnt=1):
	outDict = {}
	for f in listdir("cards\\talents"):
		outDict["cards\\talents\\"+f] = copyCnt
	return outDict




#========================================

def filterSkills(skillDict, changedStatus, changedSkills, changedSchools):
	outDict = {}
	for skl in G.SKILLS.values():
		if skl.name in changedSkills:
			outDict[skl.cardPath] = skillDict[skl.cardPath]
			continue

		if skl.scl in changedSchools:
			outDict[skl.cardPath] = skillDict[skl.cardPath]
			continue

		def sts_in_skl(changedStatus, skl):
			for sts in changedStatus:
				for ln in skl.lines:
					if sts in ln:
						return True
			return False

		if sts_in_skl(changedStatus, skl):
			outDict[skl.cardPath] = skillDict[skl.cardPath]
			continue

	for key in outDict:
		print("{}: {}".format(key, outDict[key]))

	return outDict


def filterStatus(pStatusDict,hStatusDict,sStatusDict,oStatusDict, changedStatus):

	for sts in G.STATUSES.values():
		if sts.name not in changedStatus:
			if "Power" in sts.type:
				pStatusDict[sts.cardPath]=0
			elif "Health" in sts.type:
				hStatusDict[sts.cardPath]=0
			elif "Status" in sts.type:
				sStatusDict[sts.cardPath]=0
			elif "Obstacle" in sts.type:
				oStatusDict[sts.cardPath]=0

	return pStatusDict,hStatusDict,sStatusDict,oStatusDict

