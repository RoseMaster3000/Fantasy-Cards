from os import system
from os import name as osName
from sys import stdout
import time
try: # windows
    import msvcrt as charGrab
except: # linux
    import getch as charGrab


def waitFor(inputList=[b"\r"]):
    ans = charGrab.getch()
    while ans not in inputList:
        ans = charGrab.getch()
    return ans


def printf(*arg, end="\n"):
    if "---" in end: end = "\n──────────────────────────────────────────\n"
    print(*arg, end=end)
    stdout.flush()


START = time.time()
def printProgressBar(iteration, total, suffix = '', eta = True, decimals = 1, length = 60, fill = '█'):
    # eta calculations
    if iteration==0: global START; START = time.time()
    elapsed = time.time() - START
    eta = (total - iteration + 1) * elapsed / iteration if iteration > 0 else 0
    eta_string = "{}:{:02}".format(int(eta//60), int(eta%60))

    # bar creation
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)

    #print
    print('\r |%s| %s%% (ETA: %s) %s\r' % (bar, percent, eta_string, suffix), end='')
    if iteration == total: print()


def selectColor(color=None):
    color = str(color).lower() 
    # ANSI Escape Codes
    colorDict = {
        "black"     : "\u001b[30m",
        "red"       : "\u001b[31m",
        "green"     : "\u001b[32m",
        "yellow"    : "\u001b[33m",
        "blue"      : "\u001b[34m",
        "magenta"   : "\u001b[35m",
        "cyan"      : "\u001b[36m",
        "white"     : "\u001b[37m",
        "reset"     : "\u001b[0m",
        "none"      : "\u001b[0m"
    }

    color = colorDict.get(color, '\u001b[0m')
    print(color,end="")
    return



# numeric input
# elements in [whitelist] skips sanatation
def numInput(mini=None,maxi=None, whiteList=[]):
    if mini==None: mini = float("-inf")
    if maxi==None: maxi = float("inf")

    # inputs 0-9
    if maxi <= 9 and mini >=0:
        print("\r> ", end="")
        while True:
            ans = charGrab.getch()
            if ans == b'\x03':
                raise KeyboardInterrupt
            elif ans in whiteList:
                return ans
            try:
                ans = int(ans)
                if ans>=mini and ans<=maxi:
                    print(ans)
                    return ans
            except:
                pass

    # any input (floats)
    else:
        whole = ""
        decimal = False
        negative = False
        while True:
            ans = charGrab.getch()
            # ctrl-c
            if ans == b'\x03':
                raise KeyboardInterrupt
            # backspace
            elif ans == b'\x08' and len(whole) > 0:
                if whole[-1] in'-': negative = False
                if whole[-1]=='.':  decimal = False
                print("\r"+" "*len(whole), end="", flush=False)
                whole = whole[0:-1]
                print("\r"+whole, end="", flush=True)
            # return output
            elif ans == b'\r':
                try:
                    if '.' in whole:  output = float(whole)
                    else:             output = int(whole)
                    if output <= maxi and output >= mini:
                        print("\n", end="", flush=True)
                        return output
                except:
                    continue
            # decimal char
            elif ans in [b'.'] and not decimal:
                ans = ans.decode('utf-8')
                print(ans, end="", flush=True)
                whole += ans
                decimal = True
            # negative toggle
            elif ans in [b'-'] and not negative:
                negative = True             
                print("\r"+" "*len(whole), end="", flush=False)
                whole = "-" + whole
                print("\r"+whole, end="", flush=True)
            elif ans in [b'-'] and negative:
                negative = False                
                print("\r"+" "*len(whole), end="", flush=False)
                whole = whole[1:]
                print("\r"+whole, end="", flush=True)
            # numeric input
            elif ans in [b'0',b'1',b'2',b'3',b'4',b'5',b'6',b'7',b'8',b'9']:
                ans = ans.decode('utf-8')
                print(ans, end="", flush=True)
                whole += ans




# numerates List, return element
def menu(choiceList, returnType=str):
    # sanatize choices
    if type(choiceList) == str:
        choiceList = choiceList.split(",")
        for i,item in enumerate(choiceList):
            choiceList[i] = item.strip()    
    if type(choiceList) == dict:
        choiceList = sorted(list(choiceList.keys()))

    #print menu
    for i,item in enumerate(choiceList):
        #selectColor(item)
        print("   {}. {}".format(i+1, item))
        #selectColor(None)

    # get input
    ans = numInput(1, len(choiceList))-1
    if (returnType==str):
        return choiceList[ans]
    else:
        return ans





# returns selected sublist
def multiMenu(header, choiceList, maxSelect=None):
    if maxSelect == None: maxSelect = float("inf")
    # sanatize choices
    if type(choiceList) == str:
        choiceList = choiceList.split(",")
        for i,item in enumerate(choiceList):
            choiceList[i] = item.strip()
    choiceList = list(choiceList)
    boolList = len(choiceList) * [False]

    while True:
        #print header
        clear()
        print(header, end="")
        if maxSelect != float("inf"):
            selectColor("yellow")
            print("({} more)".format(maxSelect-sum(boolList)))
            selectColor(None)
        else:
            print("")

        # print menu
        for i,item in enumerate(choiceList):
            check  = "[ ]"
            if boolList[i]: check="[X]";
            print("   {} {}. {}".format(check, i+1, item))
        
        # get input
        if len(choiceList)>=10:
            print("[d] when done")
        else:
            print("[Enter] when done")
        ans = numInput(mini=1, maxi=len(choiceList), whiteList=[b'\r', b'd'])
        
        #process input
        if ans in [b'\r', b'd']:
            return [choiceList[i] for i in range(len(choiceList)) if boolList[i]]
        else:
            ans -= 1
            boolList[ans] ^= 1
            # did we just go over the maxSelect amount? Then switch it back!
            if sum(boolList) > maxSelect:
                boolList[ans] ^= 1




def clear():
    system('cls' if osName == 'nt' else 'clear')



if __name__ == "__main__":

    for x in range(0,101):
        printProgressBar(x, 100)
        time.sleep(0.1)

    selectColor("red")
    print("Oh shit, this is red")
    selectColor()
    print("this is the defualt color again...")
    print("---")

    print("Choose one below:")
    x = menu(["Glass","Metal","Stone"])
    print("You chose", x)

    print("Choose one below:")
    x = menu("dsa,ef,gdfsg,DSAD,GBVXCB,DSADFA,EWRWE,FAFA,RFEAR,SS,FDAF,WADS,GDFSDVS")
    print("You chose", x)


    ls = multiMenu("Choose up to 3:", "African,European,Chinese,Indian,Australian", 3)
    print("You chose", ls)


    print("Type a number")
    x = numInput()
    print(x)

